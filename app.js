const Koa = require('koa');
const templating=require('./templating');

// let str=env.render('index.html',{});
// console.log(str);

let app = new Koa();

/*
    中间件特性：
    1、如果你忘记了执行await next()，那么接下来的所有的中间件将不会再执行
    2、我们可以给ctx上下文，任意的做一些处理，这个处理会影响到后面其它中间件
*/
// 引入模板引擎中间件，注册上下文渲染视图的能力
app.use(templating());

let port = 3000;

app.listen(port);

console.log(`http://localhost:${port}`);