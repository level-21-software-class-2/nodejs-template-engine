'use strict';

const nunjucks = require('nunjucks');


let env = nunjucks.configure('views', {
    noCache: true,
    watch: true
})


module.exports = function () {
    return async (ctx, next) => {
        // ctx.request.pig = '你是一个猪';
        ctx.render = function (view, data) {
            ctx.body = env.render(view, data);
        }
        await next();
    }
} 